# TODO

Here's a list of things that need to be done.

- Add commands
- Add SSL/TLS
- Literally any form of authentication would be better than what we have.
- Add messages on client disconnect in server logs
- Implement proper logging
- Add a logo for the repo (really low priority)
- Auto-Expire cached messages after TTL <-- requires re-architecting
