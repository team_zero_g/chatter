# Chatter

Chatter is a tiny chat client and server written in Python 3.6 using `sanic`, `websockets`, and `redis`.
We also have a cloud instance. Ping @Paradoxinabox for details.

## Overview

There's a server that runs the whole show.
It has 2 endpoints.

    /p # pub - publish
    /s # sub - subscribe

Users watch the room for new messages from `/s`, and publish new messages to `/p`.
Redis is used as a temporary store for messsages in transition between pub and sub.

#### pub

Users send messages to the pub endpoint. Messages are stored in a list
and output through the sub endpoint

Use `pub.py` for this

#### sub

Users listen for room activity on the sub endpoint. A '.' is sent
when no room activity has occurred.

Use `sub.py` for this

#### cache

Redis is used as a cache to store messages temporarily

## Install

    make install
    vim config.ini

## Run

run the server

    make

run the input client

    make talk

run the monitoring client

    make listen

# TODO

The TODO list can be found [here](docs/TODO.md)

## Contributing

Please create new branches for new features titled `feature/<feature_name>`,
and create a pull request to `develop` when the feature is ready to review & merge.

Also follow pep8 unless doing so would be just ridiculous in your particular scenario.
