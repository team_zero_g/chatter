import asyncio
import requests
import websockets
from datetime import datetime

from settings import host


async def listen():
    """ Used to connect to /s and listen for new messages
    """
    async with websockets.connect(f'{host}/s') as ws:
        print('[+] Connected')
        while True:
            await ws.send('.')
            m = await ws.recv()
            if m and m != '.':
                print(f'[+] {m}')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(listen())
    finally:
        loop.close()
