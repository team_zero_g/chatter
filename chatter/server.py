import asyncio
import json
import websockets
from hashlib import sha512
from datetime import datetime
from sanic import Sanic
from sanic.response import text
from chatter.settings import cache, message_key
from chatter.util import log


app = Sanic(__name__)


@app.route('/')
async def html_index(request):
    return text('[ $ ( {|  ) $ ]')


@app.websocket('/p')
async def p(request, ws):
    """
    """
    log('pub connection opened')
    while True:
        m = await ws.recv()
        await ws.send(m)
        await lock_and_load(m)
        log(f'recieved: {m}')


@app.websocket('/s')
async def s(request, ws):
    """
    """
    log('sub connection opened')
    while True:
        messages = [m.decode() for m in cache.smembers(message_key)]
        if len(messages) > 0:
            [await send_and_pop(m, ws) for m in messages]
        else:
            await ws.recv()
            await ws.send('.')


async def send_and_pop(message, ws):
    """
    """
    await ws.recv()
    await ws.send(message)
    await lock_and_remove(message)


async def lock_and_remove(message):
    cache.srem(message_key, message)


async def lock_and_load(message):
    cache.sadd(message_key, message)


def main(host='0.0.0.0', port=8000, debug=True, workers=4):
    app.run(host=host, port=port, debug=debug, workers=workers)


if __name__ == '__main__':
    main()
