import os
import redis
import configparser


here = os.path.join(os.path.dirname(os.path.realpath(__file__)))
root = os.path.join(here, '../')

config = configparser.ConfigParser()
config.read(os.path.join(root, 'config.ini'))

host = config['Default']['host']
cache = config['Default']['cache']
username = config['Default']['username']

cache = redis.StrictRedis(host=cache)
message_key = 'm'
