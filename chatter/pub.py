import asyncio
import websockets
from datetime import datetime

from settings import host, username


msg_fmt = '{} - {} - {}'.format('{}', username, '{}')


async def talk():
    """ Used to send messages to /p
    """
    async with websockets.connect(f'{host}/p') as ws:
        while True:
            m = input('[{}]: '.format(username))
            now = int(datetime.utcnow().timestamp())
            await ws.send(msg_fmt.format(now, m))
            await ws.recv()
            print('\n'*100)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(talk())
    finally:
        loop.close()
